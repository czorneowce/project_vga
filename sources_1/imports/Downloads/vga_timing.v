// File: vga_timing.v
// This is the vga timing design for EE178 Lab #4.

// The `timescale directive specifies what the
// simulation time units are (1 ns here) and what
// the simulator time step should be (1 ps here).

`timescale 1 ns / 1 ps

// Declare the module and its ports. This is
// using Verilog-2001 syntax.

module vga_timing (
  output reg [10:0] vcount,
  output reg vsync,
  output reg vblnk,
  output reg [10:0] hcount,
  output reg hsync,
  output reg hblnk,
  input wire pclk
  );

  // Describe the actual circuit for the assignment.
  // Video timing controller set for 800x600@60fps
  // using a 40 MHz pixel clock per VESA spec.
  

localparam VERTOTALTIME = 627;
localparam VERSYNCSTART = 601;
localparam VERSYNCTIME = 4;
localparam VERSYNCEND = VERSYNCSTART + VERSYNCTIME;
localparam VERBLANKSTART = 600;
localparam VERBLANKTIME = 28;
localparam VERBLANKEND = VERBLANKSTART + VERBLANKTIME;


localparam HORTOTALTIME = 1055;
localparam HORSYNCSTART = 840;
localparam HORSYNCTIME = 128;
localparam HORSYNCEND = HORSYNCSTART + HORSYNCTIME;
localparam HORBLANKSTART = 800;
localparam HORBLANKTIME = 256;
localparam HORBLANKEND = HORBLANKSTART + HORBLANKTIME;

reg [10:0] hcount_reg = 11'b0;
reg [10:0] vcount_reg = 11'b0;


 
 
always @(posedge pclk)
    begin
    if (hcount_reg == HORTOTALTIME)
        hcount_reg <= 11'b0;
    else if (hcount_reg != HORTOTALTIME)
        hcount_reg <= hcount_reg + 1;
   
    if (vcount_reg == VERTOTALTIME)
        vcount_reg <= 11'b0;
    else if (hcount_reg == HORTOTALTIME)
        vcount_reg <= vcount_reg + 1;       
    else 
        vcount_reg <= vcount_reg;
        
        
        hcount = hcount_reg;
        vcount = vcount_reg;
        hblnk = (hcount >= HORBLANKSTART && hcount < HORBLANKEND);
        vsync = (vcount >= VERSYNCSTART  && vcount < VERSYNCEND);
        hsync = (hcount >= HORSYNCSTART  && hcount <= HORSYNCEND);
        vblnk = (vcount >= VERBLANKSTART && vcount < VERBLANKEND);
    end      


     


endmodule
