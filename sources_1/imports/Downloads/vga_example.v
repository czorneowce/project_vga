// File: vga_example.v

`timescale 1 ns / 1 ps

module vga_example (
  input wire clk,
  output reg vs,
  output reg hs,
  output reg [3:0] r,
  output reg [3:0] g,
  output reg [3:0] b,
  output wire pclk_mirror,
  
  inout wire ps2_clk,
  inout wire ps2_data
  );

  wire clk_in;
  wire clk100M;
  wire locked;
  wire clk_fb;
  wire clk_ss;
  wire clk_out;
  wire pclk;
  (* KEEP = "TRUE" *) 
  (* ASYNC_REG = "TRUE" *)
  reg [7:0] safe_start = 0;
  
  clk_wiz_0 clock(
    .clk(clk),
    .clk100MHz(clk100M),
    .clk40MHz(pclk)
  );

  ODDR pclk_oddr (.Q(pclk_mirror),.C(pclk),.CE(1'b1),.D1(1'b1),.D2(1'b0),.R(1'b0),.S(1'b0) );

 ////// ------------ VGA TIMING --------- /////
 
    wire [10:0] vcount, hcount;
    wire vsync, hsync;
    wire vblnk, hblnk;
  
    vga_timing my_timing (.vcount(vcount),.vsync(vsync),.vblnk(vblnk),.hcount(hcount),.hsync(hsync),.hblnk(hblnk),.pclk(pclk));

 ////// ------------ MOUSE CTL --------- ///// 
 
    wire [11:0] mouse_xpos, mouse_ypos;
    wire [11:0] conv_xpos_out, conv_ypos_out;
 
    MouseCtl #(100000000, 500, 100) MouseCtl(
        /* in */ .ps2_clk(ps2_clk),.ps2_data(ps2_data),.clk(clk100M),
        /* out */ .xpos(mouse_xpos),.ypos(mouse_ypos)
    );
    
 ////// ------------ MOUSE CTL --------- ///// 
 
    Clock_Converter clockConv(
            .clk40Mhz(pclk), .xpos_in(mouse_xpos), .ypos_in(mouse_ypos),
            .xpos_out(conv_xpos_out), .ypos_out(conv_ypos_out)
    
    );

 ////// ------------ DRAW BACKGROUND --------- /////
 
    wire [10:0] bg_vcount_out, bg_hcount_out;
    wire bg_vsync_out, bg_hsync_out;
    wire bg_vblnk_out, bg_hblnk_out;
    wire [11:0] bg_rgb_out;
 
    draw_background draw_background (
        /* in */ .vcount_in(vcount),.vsync_in(vsync),.vblnk_in(vblnk),.hcount_in(hcount),.hsync_in(hsync),.hblnk_in(hblnk),.pclk(pclk),
        /* out */ .vcount_out(bg_vcount_out),.vsync_out(bg_vsync_out),.vblnk_out(bg_vblnk_out),.hcount_out(bg_hcount_out),.hsync_out(bg_hsync_out),.hblnk_out(bg_hblnk_out),.rgb_out(bg_rgb_out)
    );

 ////// ------------ DRAW RECT --------- /////    
   
    wire [10:0] rect_vcount_out, rect_hcount_out;
    wire rect_vsync_out, rect_hsync_out;
    wire rect_vblnk_out, rect_hblnk_out;
    wire [11:0] rect_rgb_out;
    wire [11:0] rect_pixel_out;
   
   // wire [11:0] image_rom_rgb_out;
    
    draw_rect draw_rect(
        /* in */ .vcount_in(bg_vcount_out),.vsync_in(bg_vsync_out),.vblnk_in(bg_vblnk_out),.hcount_in(bg_hcount_out),.hsync_in(bg_hsync_out),.hblnk_in(bg_hblnk_out),.rgb_in(bg_rgb_out),/*.rgb_pixel(image_rom_rgb_out),*/.xpos(conv_xpos_out),.ypos(conv_ypos_out),.pclk(pclk),
        /* out */ .vcount_out(rect_vcount_out),.vsync_out(rect_vsync_out),.vblnk_out(rect_vblnk_out),.hcount_out(rect_hcount_out),.hsync_out(rect_hsync_out),.hblnk_out(rect_hblnk_out),.rgb_out(rect_rgb_out)//,.pixel_addr(rect_pixel_out)
    );
    
 ////// ------------ IMAGE ROM --------- ///// 
 
    // image_rom image_rom( .address(rect_pixel_out),.rgb(image_rom_rgb_out),.clk(pclk));
              
 ////// ------------ MOUSE DISPLAY --------- /////   
     
    wire mouse_vsync_out, mouse_hsync_out;
    wire [10:0] mouse_vcount_out, mouse_hcount_out;
    wire mouse_vblnk_out, mouse_hblnk_out;
    wire [11:0] mouse_rgb_out; 

    MouseDisplay mouseDp(
        /* in */.pixel_clk(pclk),.xpos(conv_xpos_out),.ypos(conv_ypos_out),.hcount(rect_hcount_out),.vcount(rect_vcount_out),.red_in(rect_rgb_out[3:0]),.green_in(rect_rgb_out[7:4]),.blue_in(rect_rgb_out[11:8]),.hsync_in(rect_hsync_out),.vsync_in(rect_vsync_out),.hblnk_in(rect_hblnk_out),.vblnk_in(rect_vblnk_out),.rgb_in(rect_rgb_out), 
        /* out */.red_out(mouse_rgb_out[3:0]),.green_out(mouse_rgb_out[7:4]),.blue_out(mouse_rgb_out[11:8]),.hsync_out(mouse_hsync_out),.vsync_out(mouse_vsync_out),.hcount_out(mouse_hcount_out),.vcount_out(mouse_vcount_out)
    );
    
  always @(posedge pclk)
        begin
          {r, g, b} =  mouse_rgb_out;
             vs = mouse_vsync_out ;
             hs = mouse_hsync_out ;
           
    
    end
    
 
  
 
endmodule
