`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2018 22:20:57
// Design Name: 
// Module Name: draw_rect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module draw_rect(
   input wire hsync_in, 
   input wire hblnk_in,
   input wire vsync_in,
   input wire vblnk_in,
   input wire [10:0] hcount_in,
   input wire [10:0] vcount_in,
   input wire [11:0] rgb_in,
   input wire [11:0] xpos,
   input wire [11:0] ypos,
   input wire pclk,
   input wire [11:0] rgb_pixel,
   
   output reg [10:0] vcount_out,
   output reg vsync_out,
   output reg vblnk_out,
   output reg [10:0] hcount_out,
   output reg hsync_out,
   output reg hblnk_out,
   output reg [11:0] rgb_out,
   //output reg [11:0] pixel_addr,
   
   
   reg [11:0] color,
   integer x, y, width, height
   
   //reg vsync_out1,
   //reg vsync_out2,
   
    //reg hsync_out1,
     //reg hsync_out2,
     
    // reg hcount_out1,
      //    reg hcount_out2,
          
        //  reg vcount_out1,
          //          reg vcount_out2,
                    
    //reg vblnk_out1,
      //                reg vblnk_out2,
                      
        //               reg hblnk_out1,
          //              reg hblnk_out2,
     
   
   //wire [11:0] concate
  
       
   
    );
    
    
    
    
      //assign concate = {vcount_in - ypos[5:0],hcount_in-xpos[5:0]};
        
    
    
   always @(posedge pclk)
      begin
        // Just pass these through.
        hsync_out <= hsync_in;
        //hsync_out2 <= hsync_out1;
      //  hsync_out <= hsync_out2;
        
        vsync_out <= vsync_in;
      //  vsync_out2 <= vsync_out1;
      //  vsync_out <= vsync_out2;
        
      
      hcount_out <= hcount_in;
         //       hcount_out2 <= hcount_out1;
           //     hcount_out <= hcount_out2;
           //     
                vcount_out <= vcount_in;
          ////      vcount_out2 <= vcount_out1;
          //      vcount_out <= vcount_out2;
                
       hblnk_out <= hblnk_in;
             ////           hblnk_out2 <= hblnk_out1;
             //           hblnk_out <= hblnk_out2;
                        
                        vblnk_out <= vblnk_in;
              //          vblnk_out2 <= vblnk_out1;
              //          vblnk_out <= vblnk_out2;
        
        color <= 12'h3_4_b;
        
       //pixel_addr <= concate;
      
        width <= 48;
        height <= 64;
 
       if (hcount_in > xpos && hcount_in < (xpos+width) && vcount_in > (ypos) && vcount_in < (ypos+height)&& (vblnk_in==0)&& (hblnk_in==0))
        begin
                 
                 // rgb_out <= rgb_pixel;
                  rgb_out <= 12'h3_4_b;
                 end
                  else rgb_out <= rgb_in;      
              end
        
        
        
endmodule
