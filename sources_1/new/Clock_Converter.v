`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 18:01:14
// Design Name: 
// Module Name: Clock_Converter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Clock_Converter(
    input wire [11:0] xpos_in,
    input wire [11:0] ypos_in,
    input wire clk40Mhz,
    
    output reg [11:0] xpos_out,
    output reg [11:0] ypos_out
    );
    
    always @(posedge clk40Mhz)
          begin
          xpos_out <= xpos_in;
          ypos_out <= ypos_in;
          end
endmodule
