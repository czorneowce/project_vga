`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2018 20:57:21
// Design Name: 
// Module Name: draw_background
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`timescale 1 ns / 1 ps

module draw_background(
    input wire hsync_in, 
    input wire hblnk_in,
    input wire vsync_in,
    input wire vblnk_in,
    input wire [10:0] hcount_in,
    input wire [10:0] vcount_in,
    input wire pclk,
    
    output reg [10:0] vcount_out,
    output reg vsync_out,
    output reg vblnk_out,
    output reg [10:0] hcount_out,
    output reg hsync_out,
    output reg hblnk_out,
    output reg [11:0] rgb_out      
    );
    
     always @(posedge pclk)
     begin
       // Just pass these through.
       hsync_out = hsync_in;
       vsync_out = vsync_in;
       vcount_out = vcount_in;
       vblnk_out = vblnk_in;
       hcount_out = hcount_in;
       hblnk_out = hblnk_in;
       // During blanking, make it it black.
       if (vblnk_in || hblnk_in) {rgb_out} <= 12'h0_0_0; 
       else
       begin
     // Active display, top edge, make a yellow line.
           if (vcount_in == 0) {rgb_out} <= 12'hf_f_0;
           // Active display, bottom edge, make a red line.
           else if (vcount_in == 599) {rgb_out} <= 12'hf_0_0;
           // Active display, left edge, make a green line.
           else if (hcount_in == 0) {rgb_out} <= 12'h0_f_0;
           // Active display, right edge, make a blue line.
           else if (hcount_in == 799) {rgb_out} <= 12'h0_0_f;
           // Active display, interior, fill with gray.
           // You will replace this with your own test.
           else if (hcount_in > 50 && hcount_in < 200 && vcount_in > 50 && vcount_in < 100) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 50 && hcount_in < 100 && vcount_in > 50 && vcount_in < 250) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 50 && hcount_in < 200 && vcount_in > 200 && vcount_in < 250) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 150 && hcount_in < 200 && vcount_in > 200 && vcount_in < 400) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 50 && hcount_in < 200 && vcount_in > 350 && vcount_in < 400) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 250 && hcount_in < 450 && vcount_in > 50 && vcount_in < 100) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 400 && hcount_in < 450 && vcount_in > 50 && vcount_in < 400) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 250 && hcount_in < 450 && vcount_in > 350 && vcount_in < 400) {rgb_out} <= 12'hb_b_f;
           else if (hcount_in > 250 && hcount_in < 300 && vcount_in > 300 && vcount_in < 400) {rgb_out} <= 12'hb_b_f;
           else {rgb_out} <= 12'h8_8_8;      
       end
     end

    
endmodule
